class Product {
  String article;
  String title;
  String subtitle;
  String mainCategory;
  String manufacturer;
  dynamic img;
  int quantity;
  int cost;
  String barCode;
  int discountPercents;

  // ignore: sort_constructors_first
  Product({
    this.article,
    this.title,
    this.subtitle,
    this.mainCategory,
    this.manufacturer,
    this.img,
    this.quantity,
    this.cost,
    this.barCode,
    this.discountPercents,
  });

  Product copy() {
    return Product(
      article: article,
      title: title,
      subtitle: subtitle,
      mainCategory: mainCategory,
      manufacturer: manufacturer,
      img: img,
      quantity: quantity,
      cost: cost,
      barCode: barCode,
      discountPercents: discountPercents,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'article': article,
      'title': title,
      'subtitle': subtitle,
      'mainCategory': mainCategory,
      'manufacturer': manufacturer,
      'img': img,
      'quantity': quantity,
      'cost': cost,
      'barCode': barCode,
      'discountPercents': discountPercents,
    };
  }

  // ignore: sort_constructors_first
  Product.fromMap(Map<String, dynamic> data) {
    article = data['article'];
    title = data['title'];
    subtitle = data['subtitle'];
    mainCategory = data['mainCategory'];
    manufacturer = data['manufacturer'];
    img = data['img'];
    quantity = data['quantity'];
    cost = data['cost'];
    barCode = data['barCode'];
    discountPercents = data['discountPercents'];
  }

  Map<String, dynamic> toSimpleMap() {
    return {
      'article': article,
      'quantity': quantity,
    };
  }

  Map<String, dynamic> toOrderMap() {
    return {
      'article': article,
      'quantity': quantity,
      'cost': (cost * (100 - 0) / 100).ceil(),
    };
  }
 

  String getArticle(Map<String, dynamic> data) {
    return data['article'];
  }
}

class SimpleProduct {
  String article;
  int quantity;

  // ignore: sort_constructors_first
  SimpleProduct({
    this.article,
    this.quantity,
  });

  SimpleProduct getSimple(Product p) {
    return SimpleProduct(article: p.article, quantity: p.quantity);
  }

  Map<String, dynamic> toMap() {
    return {
      'article': article,
      'quantity': quantity,
    };
  }
}
