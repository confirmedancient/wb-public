import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:whiterosebeauty/models/category.dart';

class CategoryCard extends StatefulWidget {
  const CategoryCard({Key key, this.onTap, this.category}) : super(key: key);

  final Function onTap;
  final Category category;

  @override
  _CategoryCardState createState() => _CategoryCardState();
}

class _CategoryCardState extends State<CategoryCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      margin: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: InkWell(
        onTap: () {
          widget.onTap();
        },
        child: Stack(
          alignment: AlignmentDirectional.centerStart,
          children: [
            ExtendedImage.network(
              widget.category.img,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.circular(20)),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width * 0.35,
              fit: BoxFit.cover,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width * 0.35,
              decoration: BoxDecoration(
                  gradient: LinearGradient(stops: [
                0,
                0.5,
                0.7,
                1
              ], colors: [
                Color.fromARGB(225, 255, 255, 255),
                Color.fromARGB(225, 255, 255, 255),
                Color.fromARGB(0, 255, 255, 255),
                Color.fromARGB(0, 255, 255, 255),
              ])),
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: Text(
                      widget.category.title,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
