

!!!!!!!!!!!!!!!!!!!!!!!!!!!! CROPPED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:whiterosebeauty/components/somefunctions.dart';
import 'package:whiterosebeauty/models/adress.dart';
import 'package:whiterosebeauty/models/category.dart';
import 'package:whiterosebeauty/models/contacts.dart';
import 'package:whiterosebeauty/models/notifications.dart';
import 'package:whiterosebeauty/models/promocode.dart';
import 'package:whiterosebeauty/models/product.dart';
import 'package:whiterosebeauty/models/order.dart';
import 'package:whiterosebeauty/models/user.dart';
import 'package:whiterosebeauty/models/news.dart';

class DataBaseService {
  final CollectionReference catalog =
      FirebaseFirestore.instance.collection('catalog');

  Future addOrEditProduct(Product product) async {
    DocumentReference productRef = catalog.doc(product.article.toString());
    return productRef.set(product.toMap()).then((_) async {
      var docId = productRef.id;
      await catalog.doc(docId).set(product.toMap());
    });
  }


  Future addOrEditOrder(Order order) async {
    DocumentReference orderRef = orders.doc(order.orderId);
    return orderRef.set(order.toMap());
  }

  Future<String> getNewOrderNumber() async {
    QuerySnapshot querySnapshot = await orders.get();
    return (querySnapshot.size + 1).toString();
  }

  Future clearUserCart(UserFB userFB) async {
    DocumentReference userRef = users.doc(userFB.id);
    return userRef
        .update({'cartProducts': List<dynamic>.empty(growable: true)});
  }

  Future addOrderToUser({UserFB userFB, Order order}) async {
    DocumentReference userRef = users.doc(userFB.id);
    List<dynamic> newOrders = userFB.userData.orders;
    newOrders.add(order.orderId);
    await userRef.update({'orders': newOrders});
    return;
  }

  Future changeOrderStatus(Order order, String status) async {
    DocumentReference orderRef = orders.doc(order.orderId);
    return orderRef.update({'status': status});
  }


  Stream<List<Product>> getProducts(
      {String category, bool showOveredProducts, List articles}) {
    Query query;
    if (showOveredProducts == null) {
      query = catalog.orderBy('article');
    } else if (showOveredProducts != true) {
      query = catalog.where('quantity', isGreaterThanOrEqualTo: 1.0);
    } else {
      query = catalog.where('quantity', isLessThan: 1.0);
    }

    if (category != null) {
      query = query.where('mainCategory', isEqualTo: category);
    }

    if (articles != null) {
      if (articles.isNotEmpty) {
        query = catalog.where('article', whereIn: articles);
      }
    }
    return query.snapshots().map((QuerySnapshot data) => data.docs
        .map((DocumentSnapshot doc) => Product.fromMap(doc.data()))
        .toList());
  }

  Stream<List<News>> getNews({bool isVisible}) {
    Query query;
    if (isVisible == true) {
      query = news.where('isVisible', isEqualTo: true);
    }
    query = query.orderBy('newsId', descending: true);

    return query.snapshots().map((QuerySnapshot data) => data.docs
        .map((DocumentSnapshot doc) => News.fromMap(doc.data()))
        .toList());
  }

  Future saveUserData({UserFB userFB, Contacts contacts, Adress adress}) async {
    DocumentReference userRef = users.doc(userFB.id);

    await userRef.update({'contacts': contacts.toMap()});
    await userRef.update({'adress': adress.toMap()});
    return;
  }

  Future<Product> getProduct(String article) async {
    var doc = await catalog.doc(article).get();
    return Product.fromMap(doc.data());
  }

  Future<String> getProductImage(String article) async {
    var doc = await catalog.doc(article).get();
    return Product.fromMap(doc.data()).img;
  }

  Future<Order> getOrder(String orderNumber) async {
    var doc = await orders.doc(orderNumber).get();
    return Order.fromMap(doc.data());
  }

  Future addProductToFavorites({UserFB user, String article}) async {
    DocumentReference userRef = users.doc(user.id);
    List<dynamic> newFavorites = user.userData.favorites;

    if (!isFavoriteProduct(user: user, article: article)) {
      newFavorites.add(article);
      await userRef.update({'favorites': newFavorites});
    }
    return;
  }

  Future removeProductFromFavorites({UserFB user, String article}) async {
    DocumentReference userRef = users.doc(user.id);
    List<dynamic> newFavorites = user.userData.favorites;

    if (isFavoriteProduct(user: user, article: article)) {
      newFavorites.remove(article);
      await userRef.update({'favorites': newFavorites});
    }
    return;
  }

  Future addProductToCart({UserFB user, Product product}) async {
    var simpleProduct = <String, dynamic>{};
    var cartProducts = List<Map<String, dynamic>>.empty(growable: true);
    DocumentReference userRef = users.doc(user.id);
    List<dynamic> newCart = user.userData.cartProducts;

    int id = productIdinCart(user: user, article: product.article);
    if (id >= 0) {
      newCart[id].quantity += 1;
    } else {
      newCart.add(product);
      product.quantity = 1;
    }
    for (Product p in newCart) {
      simpleProduct = p.toSimpleMap();
      cartProducts.add(simpleProduct);
    }
    await userRef.update({'cartProducts': cartProducts});
    return;
  }

  Future subProductFromCart({UserFB user, Product product}) async {
    var simpleProduct = <String, dynamic>{};
    var cartProducts = List<Map<String, dynamic>>.empty(growable: true);
    DocumentReference userRef = users.doc(user.id);
    List<dynamic> newCart = user.userData.cartProducts;

    int id = productIdinCart(user: user, article: product.article);
    if (id >= 0) {
      if (newCart[id].quantity > 1) {
        newCart[id].quantity -= 1;
      } else {
        newCart.removeWhere((e) => e.article == product.article);
      }
      for (Product p in newCart) {
        simpleProduct = p.toSimpleMap();
        cartProducts.add(simpleProduct);
      }
      await userRef.update({'cartProducts': cartProducts});
    }
    return;
  }


  Stream<List<dynamic>> getUserFavorites(UserFB user) {
    DocumentReference userRef = users.doc(user.id);
    return userRef.snapshots().map((e) => e.get('favorites'));
  }

  Future updateUserNotifications(
      {UserFB userFB, Notifications notifications}) async {
    DocumentReference userRef = users.doc(userFB.id);
    await userRef.update({'notifications': notifications.toMap()});
    return;
  }
}
